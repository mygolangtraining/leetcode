package training

import "strconv"

func IsPalindrome(x int) bool {
	var stringX string
	var reverseX string

	stringX = strconv.Itoa(x)

	array := []rune(stringX) // convert to rune
	for i, j := 0, len(array)-1; i < j; i, j = i+1, j-1 {

		// swap the letters of the string,
		// like first with last and so on.
		array[i], array[j] = array[j], array[i]
	}

	reverseX = string(array) // return the reversed string.

	return stringX == reverseX
}
