package training

import "strings"

func LengthOfLastWord(s string) int {
	str := strings.ReplaceAll(s, " ", ",")
	arr := strings.Split(str, ",")
	str = arr[len(arr)-1]
	return len(str)
}
