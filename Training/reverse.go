package training

import (
	"strconv"
)

/*
	Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.

Assume the environment does not allow you to store 64-bit integers (signed or unsigned).

Example 1:

Input: x = 123
Output: 321
Example 2:

Input: x = -123
Output: -321
Example 3:

Input: x = 120
Output: 21
*/
func Reverse(x int) int {
	var sign int = 1

	if x < 0 {
		sign = -1
		x = x * -1
	}
	stringX := strconv.Itoa(x)

	array := []rune(stringX) // convert to rune
	for i, j := 0, len(array)-1; i < j; i, j = i+1, j-1 {

		// swap the letters of the string,
		// like first with last and so on.
		array[i], array[j] = array[j], array[i]
	}

	stringX = string(array) // return the reversed string.

	reverseX, _ := strconv.Atoi(stringX)

	return reverseX * sign
}
