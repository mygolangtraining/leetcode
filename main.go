package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	training "leetcode/Training"
	"log"
	"os"
	"strings"
)

var FileNames []string
var OutputFile *os.File

func main() {
	var exampleArray []int

	splitFile2()
	//	combineFiles(FileNames)
	fmt.Println("twoSum Example")
	exampleArray = append(exampleArray, 1, 2, 3, 6)
	fmt.Println(training.TwoSum(exampleArray, 5))

	fmt.Println("earchInsert Example")
	fmt.Println(training.SearchInsert(exampleArray, 4))

	fmt.Println("reverse Example")
	fmt.Println(training.Reverse(-536))

	fmt.Println("isPalindrome Example")
	fmt.Println(training.IsPalindrome(536))
	fmt.Println(training.IsPalindrome(1221))

	fmt.Println("lengthOfLastWord Example")
	fmt.Println(training.LengthOfLastWord("Hello World"))
	fmt.Println(training.LengthOfLastWord("   fly me   to   the moon  "))
	fmt.Println(training.LengthOfLastWord("luffy is still joyboy"))

}

func splitFile() {
	filePath := "E:\\dbase\\test\\orfeo.sql"
	chunkSize := 1073741824 //1048576 //1048576  // Her parça 1 gb
	var i int
	file, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	reader := bufio.NewReader(file)

	for {
		i++
		chunk := make([]byte, chunkSize)
		_, err := reader.Read(chunk)
		if err != nil {
			break // Dosyanın sonuna gelindi
		}

		processChunk(chunk, i)

		// Parçayı kullanarak istediğiniz işlemi gerçekleştirin

		// Örnek: Parçayı terminale yazdırma
		//fmt.Println(string(chunk))
	}
}
func splitFile2() {
	var err error

	OutputFile, err := os.OpenFile("E:\\dbase\\test\\orfeo_test.sql", os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer OutputFile.Close()

	filePath := "E:\\dbase\\test\\orfeo.sql"
	chunkSize := 107374182 //1073741824   Her parça 1 gb
	var i int
	file, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	reader := bufio.NewReader(file)

	for {
		i++
		chunk := make([]byte, chunkSize)
		_, err := reader.Read(chunk)
		if err != nil {
			break // Dosyanın sonuna gelindi
		}

		processChunk2(chunk, i)

		// Parçayı kullanarak istediğiniz işlemi gerçekleştirin

		// Örnek: Parçayı terminale yazdırma
		//fmt.Println(string(chunk))
	}
}

func processChunk(chunk []byte, pindex int) {
	// Parça üzerinde yapılacak işlem
	var byteArray []byte
	//var line string
	lines := strings.Split(string(chunk), "\n")

	// Satırları terminale yazdır
	for _, line := range lines {
		line += "test"
		byteArray = append(byteArray, []byte(line)...)
		//byteArray = []byte(line)
	}

	// Bu fonksiyonu ihtiyaçlarınıza göre düzenleyebilirsiniz
	wFilePath := fmt.Sprintf("E:\\dbase\\test\\output%v.txt", pindex)
	FileNames = append(FileNames, wFilePath)
	err := ioutil.WriteFile(wFilePath, byteArray, 0644)
	if err != nil {
		log.Fatal(err)
	}
}
func processChunk2(chunk []byte, pindex int) {
	// Parça üzerinde yapılacak işlem
	// var byteArray []byte
	// var linestr string
	lines := strings.Split(string(chunk), "\n")

	// Satırları terminale yazdır
	for _, line := range lines {
		line += "test"

		_, err := OutputFile.WriteString(line)
		if err != nil {
			log.Fatal(err)
		}
	}

	// Bu fonksiyonu ihtiyaçlarınıza göre düzenleyebilirsiniz
	//wFilePath := "E:\\dbase\\test\\orfeo_test.sql"
	//FileNames = append(FileNames, wFilePath)
	// linestr = string(byteArray)
	// _, err := OutputFile.WriteString(linestr)
	// //err := ioutil.WriteFile(wFilePath, byteArray, 0644)
	// if err != nil {
	// 	log.Fatal(err)
	// }
}
func combineFiles(fileNames []string) {
	outputFile, err := os.Create("E:\\dbase\\test\\orfeo_test.sql")
	if err != nil {
		log.Fatal(err)
	}
	defer outputFile.Close()

	//fileNames := []string{"dosya1.txt", "dosya2.txt", "dosya3.txt"} // Birleştirmek istediğiniz dosya isimleri

	for _, fileName := range fileNames {
		inputFile, err := os.Open(fileName)
		if err != nil {
			log.Fatal(err)
		}
		defer inputFile.Close()

		_, err = io.Copy(outputFile, inputFile)
		if err != nil {
			log.Fatal(err)
		}

		//	fmt.Printf("%s dosyası birleştirildi.\n", fileName)
	}

	fmt.Println("Dosyalar başarıyla birleştirildi.")
}
func writeBigFile2() {
	// Okunacak dosyamızı belirtiyoruz
	dosya, err := ioutil.ReadFile("E:\\dbase\\orfeo.sql")
	// Hata kontrolü yapıyoruz.
	if err != nil {
		log.Fatal(err)
	}
	// İçeriği satırlara ayır
	lines := strings.Split(string(dosya), "\n")

	// Satırları terminale yazdır
	for _, line := range lines {
		fmt.Println(line)
	}

	//Dosyamızın içeriğini ekrana bastırıyoruz.
	fmt.Println(string(dosya))
	if err != nil {
		log.Fatal(err)
	}
}
func writeBigFile() {
	filePath := "E:\\dbase\\orfeo.sql"

	// Orjinal dosya
	originalFile, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	defer originalFile.Close()

	// Geçici dosya
	tempFilePath := "temp.txt"
	tempFile, err := os.Create(tempFilePath)
	if err != nil {
		log.Fatal(err)
	}
	defer tempFile.Close()

	scanner := bufio.NewScanner(originalFile)
	writer := bufio.NewWriter(tempFile)

	// Satır satır oku ve işle
	for scanner.Scan() {
		line := scanner.Text()

		// İstenilen işlemi yap
		processedLine := processLine(line)

		// Geçici dosyaya yaz
		_, err := fmt.Fprintln(writer, processedLine)
		if err != nil {
			log.Fatal(err)
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	// Yazma tamamlanmadan önce buffer'ı boşalt
	if err := writer.Flush(); err != nil {
		log.Fatal(err)
	}

	// Dosyaları kapat
	if err := originalFile.Close(); err != nil {
		log.Fatal(err)
	}
	if err := tempFile.Close(); err != nil {
		log.Fatal(err)
	}

	// Orjinal dosyayı sil
	err = os.Remove(filePath)
	if err != nil {
		log.Fatal(err)
	}

	// Geçici dosyayı orjinal dosya adıyla yeniden adlandır
	err = os.Rename(tempFilePath, filePath)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Dosya başarıyla güncellendi.")
}

func processLine(line string) string {
	// İşlem yapılacak satırın örneği
	// Bu fonksiyonu kendi ihtiyaçlarınıza göre düzenleyebilirsiniz
	processedLine := line + " (processed)"

	return processedLine
}
func writeFile() {
	// Dosyayı okuma
	filePath := "E:\\dbase\\orfeo.sql"
	file, err := os.OpenFile(filePath, os.O_RDWR, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	lines := make([]string, 0)

	// Satırları oku ve işle
	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	// İstenilen satır üzerine yaz
	lineIndex := 2 // Üzerine yazılacak satırın indeksi (0'dan başlar)

	if lineIndex >= 0 && lineIndex < len(lines) {
		lines[lineIndex] = "Yeni içerik"
	} else {
		log.Fatal("Geçersiz satır indeksi")
	}

	// Dosyaya yazma
	file, err = os.OpenFile(filePath, os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	writer := bufio.NewWriter(file)
	for _, line := range lines {
		fmt.Fprintln(writer, line)
	}

	if err := writer.Flush(); err != nil {
		log.Fatal(err)
	}

	fmt.Println("Satır başarıyla güncellendi.")
}
